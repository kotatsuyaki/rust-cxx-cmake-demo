fn main() {
    let foo = ffi::new_foo();
}

#[cxx::bridge]
mod ffi {
    unsafe extern "C++" {
        include!("rust-cxx-playground/include/foo.hpp");

        type Foo;

        fn new_foo() -> UniquePtr<Foo>;
    }
}
